package com.example.alumno.dbdandriod;

/**
 * Created by alumno on 24/07/2017.
 */

public class Asignatura {
    private String nombre;
    private int cantidadEstudiantes, codigo;

    public Asignatura() {
    }

    public Asignatura(String nombre, int cantidadEstudiantes, int codigo) {
        this.nombre = nombre;
        this.cantidadEstudiantes = cantidadEstudiantes;
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidadEstudiantes() {
        return cantidadEstudiantes;
    }

    public void setCantidadEstudiantes(int cantidadEstudiantes) {
        this.cantidadEstudiantes = cantidadEstudiantes;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "codigo: "+codigo+" Nombre:"+nombre+" Cantidad de Estudiante: "+cantidadEstudiantes ;
    }
}
