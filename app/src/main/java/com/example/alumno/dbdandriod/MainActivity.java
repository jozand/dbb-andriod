package com.example.alumno.dbdandriod;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Asignatura> asignaturas;
    private ArrayAdapter<Asignatura> adapter;
    private AdminSQLiteOpenHelper admin;

    //Elementos de entrada de la vista
    private EditText nomAsig;
    private EditText cantEstudiantes,codAsig;
    private Button bt1;
    private Button bt2;
    private Button bt3;
    private ListView listIngreso;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nomAsig = (EditText) findViewById(R.id.nomAsig);
        cantEstudiantes = (EditText) findViewById(R.id.cantEstudiantes);
        codAsig = (EditText) findViewById(R.id.codAsig);
        bt1 = (Button) findViewById(R.id.bt1);
        bt2 = (Button) findViewById(R.id.bt2);
        bt3 = (Button) findViewById(R.id.bt3);
        listIngreso = (ListView) findViewById(R.id.listIngreso);

        asignaturas = new ArrayList<>();
        adapter = new ArrayAdapter<Asignatura>(this, android.R.layout.simple_list_item_1, asignaturas);
        listIngreso.setAdapter(adapter);
        admin = new AdminSQLiteOpenHelper(this, "administracion", null, 1);

    }
    public void add(View view){
        SQLiteDatabase db = admin.getWritableDatabase();
        int codigo=Integer.parseInt(codAsig.getText().toString());
        int cantAl=Integer.parseInt(cantEstudiantes.getText().toString());
        String nombre = nomAsig.getText().toString();
        ContentValues registros = new ContentValues();
        registros.put("codigo",codigo);
        registros.put("nombre",nombre);
        registros.put("cantidadEstudiantes",cantAl);
        db.insert("asignatura",null,registros);
        db.close();
        Toast.makeText(this,"Asignatura Ingresada", Toast.LENGTH_LONG).show();
        showAll();
    }

    public void delete(View view){
        SQLiteDatabase db = admin.getReadableDatabase();
        String codigo =codAsig.getText().toString();
        int cant= db.delete("asignatura","codigo="+codigo,null);
        db.close();
        if (cant==1)
            Toast.makeText(this,"Asignatura Eliminada", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this,"No existe asignatura con ese codigo", Toast.LENGTH_LONG).show();

        showAll();
    }
    public void showAll(){
        String query="select *from asignatura";
        SQLiteDatabase db =admin.getReadableDatabase();
        asignaturas.clear();
        Cursor c = db.rawQuery(query,null);
        while(c.moveToNext()){
            Asignatura a = new Asignatura();
            a.setCodigo(c.getInt(c.getColumnIndex("codigo")));
            a.setNombre(c.getString(c.getColumnIndex("nombre")));
            a.setCantidadEstudiantes(c.getInt(c.getColumnIndex("cantidadEstudiantes")));
            asignaturas.add(a);
        }
        adapter.notifyDataSetChanged();

    }

}
